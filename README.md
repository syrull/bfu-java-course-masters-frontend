# Java базирани софтуерни системи

# Изработка

Създаден е Front-End, което представлява много проста система за добавяне на: 
Студентски Програми, Студенти и Факултети.

# Използвани Технологии

* Angular 7
* Angular Material UI

# Изглед

![students](https://raw.githubusercontent.com/f0xxx1/university-app-students/master/assets-github/front-end/students.png)
