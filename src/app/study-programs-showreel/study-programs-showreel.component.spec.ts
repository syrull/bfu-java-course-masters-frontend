import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyProgramsShowreelComponent } from './study-programs-showreel.component';

describe('StudyProgramsShowreelComponent', () => {
  let component: StudyProgramsShowreelComponent;
  let fixture: ComponentFixture<StudyProgramsShowreelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyProgramsShowreelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyProgramsShowreelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
