import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Main 
import { AppComponent } from './app.component';

// Students
import { StudentsComponent } from './students/students.component';
import { StudentsDetailComponent } from './students-detail/students-detail.component';
import { StudentsAddComponent } from './students-add/students-add.component';
import { StudentsEditComponent } from './students-edit/students-edit.component';

// Faculties 
import { FacultiesComponent } from './faculties/faculties.component';
import { FacultiesDetailComponent } from './faculties-detail/faculties-detail.component';
import { FacultiesAddComponent } from './faculties-add/faculties-add.component';
import { FacultiesEditComponent } from './faculties-edit/faculties-edit.component';

// Student Programs 
import { StudyProgramsComponent } from './study-programs/study-programs.component';
import { StudyProgramsDetailComponent } from './study-programs-detail/study-programs-detail.component';
import { StudyProgramsAddComponent } from './study-programs-add/study-programs-add.component';
import { StudyProgramsEditComponent } from './study-programs-edit/study-programs-edit.component';

// Pages
import { HomeComponent } from './home/home.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        // Route: /students/
        path: 'students',
        component: StudentsComponent,
        data: { title: 'Студенти' }
      },
      {
        path: 'students/details/:id',
        component: StudentsDetailComponent,
        data: { title: 'Студент' }
      },
      {
        path: 'students/add',
        component: StudentsAddComponent,
        data: { title: 'Добави Студент' }
      },
      {
        path: 'students/edit/:id',
        component: StudentsEditComponent,
        data: { title: 'Редактирай Студент' }
      },

      // Route: /faculties/
      {
        path: 'faculties',
        component: FacultiesComponent,
        data: { title: 'Факултети' }
      },
      {
        path: 'faculties/details/:id',
        component: FacultiesDetailComponent,
        data: { title: 'Факултет' }
      },
      {
        path: 'faculties/add',
        component: FacultiesAddComponent,
        data: { title: 'Добави Факултет' }
      },
      {
        path: 'faculties/edit/:id',
        component: FacultiesEditComponent,
        data: { title: 'Редактирай Факултет' }
      },

      // Route: /study-programs/
      {
        path: 'study-programs',
        component: StudyProgramsComponent,
        data: { title: 'Факултети' }
      },
      {
        path: 'study-programs/details/:id',
        component: StudyProgramsDetailComponent,
        data: { title: 'Факултет' }
      },
      {
        path: 'study-programs/add',
        component: StudyProgramsAddComponent,
        data: { title: 'Добави Факултет' }
      },
      {
        path: 'study-programs/edit/:id',
        component: StudyProgramsEditComponent,
        data: { title: 'Редактирай Факултет' }
      },
    ],
    data: { title: 'Начало' }
  },


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
