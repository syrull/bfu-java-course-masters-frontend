import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { ApiService } from '../../api.service';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

import { DialogStudentSuccessfullyAdded } from "../dialogs/dialog-student-successfully-added.component"

@Component({
  selector: 'app-faculties-add',
  templateUrl: './faculties-add.component.html',
  styleUrls: ['./faculties-add.component.css']
})
export class FacultiesAddComponent implements OnInit {

  facultyForm: FormGroup;

  name: string = '';


  isLoadingResults = false;
  showError = false;

  durationInSeconds = 5;

  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder, private snackBar: MatSnackBar) {
    this.facultyForm = this.formBuilder.group({
      'name': [this.name, Validators.compose([
        Validators.required,
        Validators.maxLength(255),
        Validators.minLength(5),
      ])]
    });
  }

  ngOnInit() {

  }

  openSnackBar() {
    this.snackBar.openFromComponent(DialogStudentSuccessfullyAdded, {
      duration: this.durationInSeconds * 1000,
      panelClass: ['panel-success']
    });
  }

  onFormSubmit(form: NgForm) {
    this.isLoadingResults = true;
    this.api.addFaculty(form)
      .subscribe(res => {
        this.openSnackBar();
        this.isLoadingResults = false;
        this.router.navigate(['faculties']);
      }, (err) => { 
        this.showError = true;
        this.isLoadingResults = false;
      });
  }

}
