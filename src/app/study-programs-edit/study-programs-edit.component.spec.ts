import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StudyProgramsEditComponent } from './study-programs-edit.component';

describe('StudyProgramsEditComponent', () => {
  let component: StudyProgramsEditComponent;
  let fixture: ComponentFixture<StudyProgramsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudyProgramsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StudyProgramsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
