import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

// Material Theme
import {
  MatInputModule,
  MatPaginatorModule,
  MatProgressSpinnerModule,
  MatSortModule,
  MatTableModule,
  MatIconModule,
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatDividerModule,
  MatOptionModule,
  MatSelectModule,
  MatSnackBarModule,
  MatMenuModule,
  MatTreeModule,
} from "@angular/material";
import { MatToolbarModule } from '@angular/material/toolbar'; 
import { MatExpansionModule } from '@angular/material/expansion'; 
import { NgSelectModule } from '@ng-select/ng-select';
import {MDCSelect, MDCSelectFoundation} from '@material/select';

// For REST Service
import { FormsModule, ReactiveFormsModule  } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// Students
import { StudentsComponent } from './students/students.component';
import { StudentsDetailComponent } from './students-detail/students-detail.component';
import { StudentsAddComponent } from './students-add/students-add.component';
import { StudentsEditComponent } from './students-edit/students-edit.component';

// Faculties 
import { FacultiesComponent } from './faculties/faculties.component';
import { FacultiesDetailComponent } from './faculties-detail/faculties-detail.component';
import { FacultiesAddComponent } from './faculties-add/faculties-add.component';
import { FacultiesEditComponent } from './faculties-edit/faculties-edit.component';

// Student Programs 
import { StudyProgramsComponent } from './study-programs/study-programs.component';
import { StudyProgramsDetailComponent } from './study-programs-detail/study-programs-detail.component';
import { StudyProgramsAddComponent } from './study-programs-add/study-programs-add.component';
import { StudyProgramsEditComponent } from './study-programs-edit/study-programs-edit.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Utils 
import { NavbarComponent } from './navbar/navbar.component';
import { DialogStudentSuccessfullyAdded } from './dialogs/dialog-student-successfully-added.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { StudyProgramsShowreelComponent } from './study-programs-showreel/study-programs-showreel.component';

@NgModule({
  declarations: [
    AppComponent,
    StudentsComponent,
    StudentsDetailComponent,
    StudentsAddComponent,
    StudentsEditComponent,
    FacultiesComponent,
    FacultiesDetailComponent,
    FacultiesAddComponent,
    FacultiesEditComponent,
    StudyProgramsComponent,
    StudyProgramsDetailComponent,
    StudyProgramsAddComponent,
    StudyProgramsEditComponent,
    NavbarComponent,
    DialogStudentSuccessfullyAdded,
    HeaderComponent,
    HomeComponent,
    StudyProgramsShowreelComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatDividerModule,
    MatExpansionModule,
    MatExpansionModule,
    MatSelectModule,
    MatOptionModule,
    NgSelectModule,
    MatSnackBarModule,
    MatMenuModule,
    MatTreeModule,
  ],
  providers: [],
  entryComponents: [
    DialogStudentSuccessfullyAdded
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
