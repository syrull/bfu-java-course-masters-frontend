import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacultiesDetailComponent } from './faculties-detail.component';

describe('FacultiesDetailComponent', () => {
  let component: FacultiesDetailComponent;
  let fixture: ComponentFixture<FacultiesDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacultiesDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacultiesDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
