import { Component, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { ApiService } from '../../api.service';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';

import { StudyProgram } from "../study-programs/study-program";
import { DialogStudentSuccessfullyAdded } from "../dialogs/dialog-student-successfully-added.component"

import { FormOfEducation, Year } from "../interfaces/utils";

@Component({
  selector: 'app-students-add',
  templateUrl: './students-add.component.html',
  styleUrls: ['./students-add.component.css']
})
export class StudentsAddComponent implements OnInit {

  studentForm: FormGroup;

  firstName: string = '';
  lastName: string = '';
  studentNumber: number = null;
  yearOfStudy: number = null;
  formOfEducation: number = null;
  studyProgramId: number = null;

  studyPrograms: StudyProgram[] = [];

  years: Year[] = [
    { value: 1, viewValue: '1' },
    { value: 2, viewValue: '2' },
    { value: 3, viewValue: '3' },
    { value: 4, viewValue: '4' },
    { value: 5, viewValue: '5' },
    { value: 6, viewValue: '6' }
  ];

  formOfEducations: FormOfEducation[] = [
    { value: 1, viewValue: 'Редовно' },
    { value: 2, viewValue: 'Задочно' }
  ];


  isLoadingResults = false;
  showError = false;

  durationInSeconds = 5;

  constructor(private router: Router, private api: ApiService, private formBuilder: FormBuilder, private snackBar: MatSnackBar) {
    this.studentForm = this.formBuilder.group({
      'firstName': [this.firstName, Validators.compose([
        Validators.required,
        Validators.maxLength(255),
        Validators.minLength(5),
      ])],
      'lastName': [this.lastName, Validators.required],
      'studentNumber': [this.studentNumber, Validators.compose([
        Validators.required,
        Validators.maxLength(20),
        Validators.minLength(5),
      ])],
      'yearOfStudy': [this.yearOfStudy, Validators.required],
      'formOfEducation': [this.formOfEducation, Validators.required],
      'studyProgram': this.formBuilder.group({
        'id': [this.studyProgramId, Validators.required]
      })
    });
  }

  ngOnInit() {
    this.api.getStudyPrograms().subscribe(studyPrograms => {
      this.studyPrograms = studyPrograms;
    });
  }

  openSnackBar() {
    this.snackBar.openFromComponent(DialogStudentSuccessfullyAdded, {
      duration: this.durationInSeconds * 1000,
      panelClass: ['panel-success']
    });
  }

  onFormSubmit(form: NgForm) {
    this.isLoadingResults = true;
    this.api.addStudent(form)
      .subscribe(res => {
        this.openSnackBar();
        this.isLoadingResults = false;
        this.router.navigate(['students']);
      }, (err) => {
        this.showError = true;
        this.isLoadingResults = false;
      });
  }


}
