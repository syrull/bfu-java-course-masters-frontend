export class Student {
  id: number;
  firstName: string;
  lastName: string;
  studentNumber: number;
  yearOfStudy: number;
  formOfEducation: number;
  studyProgram: {
    id: number
    name: string
  };
}
// https://www.djamware.com/post/5bca67d780aca7466989441f/angular-7-tutorial-building-crud-web-application
