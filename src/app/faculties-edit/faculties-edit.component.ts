import { MatSnackBar } from '@angular/material';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Faculty } from '../faculties/faculty';
import { ApiService } from '../../api.service';
import { DialogStudentSuccessfullyAdded } from "../dialogs/dialog-student-successfully-added.component"

@Component({
  selector: 'app-faculties-edit',
  templateUrl: './faculties-edit.component.html',
  styleUrls: ['./faculties-edit.component.css']
})
export class FacultiesEditComponent implements OnInit {

  facultyForm: FormGroup;
  faculty: Faculty = {
    id: null,
    name: null
  };
  name: string = '';

  isLoadingResults = false;
  showError = false;

  durationInSeconds = 5;
  id = null;

  constructor(
    private route: 
    ActivatedRoute, 
    private router: Router, 
    private api: ApiService, 
    private formBuilder: FormBuilder, 
    private snackBar: MatSnackBar
    ) {
    this.facultyForm = this.formBuilder.group({
      'name': [this.name, Validators.compose([
        Validators.required,
        Validators.maxLength(255),
        Validators.minLength(5),
      ])]
    });
  }

  getFacultyDetails(id: number) {
    this.api.getFaculty(id)
      .subscribe(data => {
        this.faculty = data;
        console.log(this.faculty);
        this.isLoadingResults = false;
      });
  }

  ngOnInit() {
    this.getFacultyDetails(this.route.snapshot.params['id']);
  }

  openSnackBar() {
    this.snackBar.openFromComponent(DialogStudentSuccessfullyAdded, {
      duration: this.durationInSeconds * 1000,
      panelClass: ['panel-success']
    });
  }

  onFormSubmit(form: NgForm) {
    this.id = this.route.snapshot.params['id'];
    this.isLoadingResults = true;
    this.api.updateFaculty(this.id, form)
      .subscribe(res => {
        this.openSnackBar();
        this.isLoadingResults = false;
        this.router.navigate(['faculties']);
      }, (err) => { 
        this.showError = true;
        this.isLoadingResults = false;
      });
  }

}
